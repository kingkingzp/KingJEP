/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.0.27-community-nt : Database - kingjep
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`kingjep` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `kingjep`;

/*Table structure for table `code_table` */

DROP TABLE IF EXISTS `code_table`;

CREATE TABLE `code_table` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `CODE` varchar(32) default NULL,
  `PARENT_ID` varchar(32) NOT NULL,
  `TYPE` varchar(32) default NULL,
  `REMARK` varchar(100) default NULL,
  `suosu_deptid` varchar(50) NOT NULL,
  `suosu_deptname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `code_table` */

insert  into `code_table`(`ID`,`NAME`,`CODE`,`PARENT_ID`,`TYPE`,`REMARK`,`suosu_deptid`,`suosu_deptname`) values ('8a81850534a6998a0134a6b4ce480002','用户属性','','','0','','',''),('8a8185a538dabcec0138dae673860004','研究生','undefined','8a8185a538dabcec0138db9330370009','0','','',''),('8a8185a538dabcec0138db9330370009','学历','','8a81850534a6998a0134a6b4ce480002','0','','',''),('8a8185a538dabcec0138dba56e16102b','年龄段',NULL,'',NULL,NULL,'',''),('402880e53a24e88b013a24ecdfe30002','90后','undefined','8a8185a538dabcec0138dba56e16102b','0','','',''),('402880e53a24e88b013a24ef20ca0005','80后','undefined','8a8185a538dabcec0138dba56e16102b','0','','',''),('402880e53a2e423e013a2e536ff70002','70后','undefined','8a8185a538dabcec0138dba56e16102b','0','','',''),('402880923fc8972b013fc8c102eb0006','本科生','undefined','8a8185a538dabcec0138db9330370009','0','','',''),('402880923fc8972b013fc8c1356f0007','专科生','undefined','8a8185a538dabcec0138db9330370009','0','','',''),('402880923fc8972b013fc8c22e4b0008','00后','undefined','8a8185a538dabcec0138dba56e16102b','0','','',''),('402881ea4ecea69b014eceb880750012','行业','','8a81850534a6998a0134a6b4ce480002','0','','',''),('402881ea4ecea69b014eceb898620013','计算机/互联网/通信/电子','','402881ea4ecea69b014eceb880750012','0','','',''),('402881ea4ecea69b014eceb8b7050014','会计/金融/银行/保险','','402881ea4ecea69b014eceb880750012','0','','',''),('402881ea4ecea69b014eceb8e3cd0015','贸易/消费/制造/营运','','402881ea4ecea69b014eceb880750012','0','','',''),('402881ea4ecea69b014eceb934650016','制药/医疗','','402881ea4ecea69b014eceb880750012','0','','',''),('402881ea4ecea69b014eceb94fc40017','广告/媒体','','402881ea4ecea69b014eceb880750012','0','','',''),('402881ea4ecea69b014eceb969bc0018','房地产/建筑','','402881ea4ecea69b014eceb880750012','0','','',''),('402881ea4ecea69b014eceb987540019','专业服务/教育/培训','','402881ea4ecea69b014eceb880750012','0','','',''),('402881ea4ecea69b014eceb99fa4001a','服务业','','402881ea4ecea69b014eceb880750012','0','','',''),('402881ea4ecea69b014eceb9b7e5001b','物流/运输','','402881ea4ecea69b014eceb880750012','0','','',''),('402881ea4ecea69b014eceb9d2ed001c','能源/原材料','','402881ea4ecea69b014eceb880750012','0','','',''),('402881ea4ecea69b014eceb9f305001d','政府/非盈利机构/其他','','402881ea4ecea69b014eceb880750012','0','','',''),('402881ea4ecea69b014eceba4e1d001e','性别','','8a81850534a6998a0134a6b4ce480002','0','','',''),('402881ea4ecea69b014eceba76ae001f','男','','402881ea4ecea69b014eceba4e1d001e','0','','',''),('402881ea4ecea69b014eceba89100020','女','','402881ea4ecea69b014eceba4e1d001e','0','','',''),('8a81850534a6998a0134a6b4ce480011','业务角色',NULL,'',NULL,NULL,'',''),('402881ea4ecea69b014ecebe1d33002a','管理','','8a81850534a6998a0134a6b4ce480011','0','','',''),('402881ea4ecea69b014ecebe3363002b','助理','','8a81850534a6998a0134a6b4ce480011','0','','',''),('402881ea4ecea69b014ecebe6245002c','经理','','8a81850534a6998a0134a6b4ce480011','0','','',''),('402881ea4ecea69b014ecebe7d7e002d','销售','','8a81850534a6998a0134a6b4ce480011','0','','',''),('402881ea4ecea69b014ecebe95cf002e','行政','','8a81850534a6998a0134a6b4ce480011','0','','',''),('297e9e795127c610015127c7d9ba0002','分公司负责人','','8a81850534a6998a0134a6b4ce480011','0','','',''),('297e9e795127c610015127c7fc600003','总公司负责人','','8a81850534a6998a0134a6b4ce480011','0','','','');

/*Table structure for table `contentcategory` */

DROP TABLE IF EXISTS `contentcategory`;

CREATE TABLE `contentcategory` (
  `CATEGORYID` varchar(50) NOT NULL,
  `CATEGORYNAME` varchar(50) default NULL,
  `PARENTID` varchar(50) default NULL,
  `rank` varchar(50) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `contentcategory` */

insert  into `contentcategory`(`CATEGORYID`,`CATEGORYNAME`,`PARENTID`,`rank`) values ('402881e53509f80b01350a5ed26b0004','通知通告',NULL,'1');

/*Table structure for table `kms_affiche` */

DROP TABLE IF EXISTS `kms_affiche`;

CREATE TABLE `kms_affiche` (
  `AFFICHE_ID` varchar(50) NOT NULL,
  `AFFICHE_TITLE` varchar(1000) default NULL,
  `AFFICHE_CONTENT` mediumtext,
  `AFFICHE_STARTDATE` varchar(20) default NULL,
  `AFFICHE_ENDDATE` varchar(20) default NULL,
  `AFFICHE_STATE` varchar(2) default NULL,
  `AFFICHE_CREATOR` varchar(32) default NULL,
  `AFFICHE_CREATEDATE` varchar(20) default NULL,
  `AFFICHE_LASTEDITOR` varchar(32) default NULL,
  `AFFICHE_LASTEDITDATE` varchar(20) default NULL,
  `DEP_ID` varchar(50) default NULL,
  `CATEGORY_ID` varchar(50) default NULL,
  `SYSROLE_IDS` varchar(2000) default NULL,
  `FJ_NAME` varchar(100) default NULL,
  `FJ_DIR` varchar(400) default NULL,
  `AFFIX_NAMESTR` varchar(2000) default NULL,
  `AFFICHE_USERS` varchar(4000) default NULL,
  `AFFICHE_USERSNAMES` varchar(4000) default NULL,
  `CREATOR_LOGINNAME` varchar(20) default NULL,
  `PK_USERID` varchar(32) default NULL,
  `PK_USERNAME` varchar(10) default NULL,
  `PK_CRATEDATE` varchar(50) default NULL,
  `PANKOU_JINBI` varchar(30) default NULL,
  `PK_GUANDIAN` varchar(10) default NULL,
  `PK_STATU` varchar(10) default NULL,
  `PK_USERNICHENG` varchar(20) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `kms_affiche` */

insert  into `kms_affiche`(`AFFICHE_ID`,`AFFICHE_TITLE`,`AFFICHE_CONTENT`,`AFFICHE_STARTDATE`,`AFFICHE_ENDDATE`,`AFFICHE_STATE`,`AFFICHE_CREATOR`,`AFFICHE_CREATEDATE`,`AFFICHE_LASTEDITOR`,`AFFICHE_LASTEDITDATE`,`DEP_ID`,`CATEGORY_ID`,`SYSROLE_IDS`,`FJ_NAME`,`FJ_DIR`,`AFFIX_NAMESTR`,`AFFICHE_USERS`,`AFFICHE_USERSNAMES`,`CREATOR_LOGINNAME`,`PK_USERID`,`PK_USERNAME`,`PK_CRATEDATE`,`PANKOU_JINBI`,`PK_GUANDIAN`,`PK_STATU`,`PK_USERNICHENG`) values ('402880e4406abc1501406abee5560001','来自客户端盘口消息','北极熊濒临灭绝，我看不远啦！','1376180430096','2013-10-30 23:59:00','','4028808113debac70113dec10a4d0005','1376180430096','','1376180430096','','402880923fc8972b013fc8e12041001d','','','','',NULL,NULL,'admin','4028808113debac70113dec10a4d0005','admin','1376196406008','100','1','2','如意帮主'),('402880e4406abc1501406ad7983c0005','来自客户端盘口消息','中国足球能垫底多少年？','1376182048826','2013-10-30 23:59:00','','4028808113debac70113dec10a4d0005','1376182048826','','1376182048826','','402880923fc8972b013fc8e12041001d','','','','',NULL,NULL,'admin','4028808113debac70113dec10a4d0005','admin','1376197293085','100','3','3','如意帮主'),('402880e4406abc1501406ae2b93c0007','来自客户端盘口消息','地球还能转一万年，我坚信！','1376182778171','2013-10-30 23:59:00','','4028808113debac70113dec10a4d0005','1376182778171','','1376182778171','','402880923fc8972b013fc8e12041001d','','','','',NULL,NULL,'admin','4028808113debac70113dec10a4d0005','admin','1376196410792','100','1','2','如意帮主'),('402880e4406abc1501406aed1e500009','来自客户端盘口消息','爱你一万年，不变，你觉得呢！','1376183459406','2013-10-30 23:59:00','','4028808113debac70113dec10a4d0005','1376183459406','','1376183459406','','402880923fc8972b013fc8e12041001d','','','','',NULL,NULL,'admin','4028808113debac70113dec10a4d0005','admin','1376197280407','100','3','3','如意帮主'),('402880e4406b2cfa01406bd6214d0003','来自客户端盘口消息','天气如此闷热，引无数英雄来PK......','1376198730059','2013-10-30 23:59:00','','4028808113debac70113dec10a4d0005','1376198730059','','1376198730059','','402880923fc8972b013fc8e12041001d','','','','',NULL,NULL,'admin','4028808113debac70113dec10a4d0005','admin','1376294994301','100','2','3','如意帮主'),('402880e440c98e410140c9fba8ea0003','来自客户端盘口消息','明天股市收盘价1800点。','1377778247911','2013-10-30 23:59:00','','402880923fc8972b013fc8cfc152000c','1377778247911','','1377778247911','','402880923fc8972b013fc8e12041001d','','','','',NULL,NULL,'jinzp','402880923fc8972b013fc8cfc152000c','jinzp','1383137080308','100','4','4','Golden金'),('402880e440c98e410140c9fcf1c70005','来自客户端盘口消息','我今天确实抽了20根烟！你信吗？','1377778332099','2013-10-30 23:59:00','','402880923fc8972b013fc8d06a0d0010','1377778332099','','1377778332099','','402880923fc8972b013fc8e12041001d','','','','',NULL,NULL,'jiangliang','402880923fc8972b013fc8d101f40012','yanglei','1377778537604','100','1','2','卡卡磊'),('402880e440c98e410140c9ff89470007','来自客户端盘口消息','@来自微博第三方:明天晚上网球赛八分之一晋级赛，李娜必进！','1377778501957','2013-10-30 23:59:00','','402880923fc8972b013fc8d101f40012','1377778501957','','1377778501957','','402880923fc8972b013fc8e12041001d','','','','',NULL,NULL,'yanglei','','','','100','','1',NULL),('402881e94208c7b30142096545270030','来自客户端盘口消息','wowow','1383137101093','2080-10-04 00:00:00','','402880923fc8972b013fc8cfc152000c','1383137101093','','1383137101093','','402880923fc8972b013fc8e12041001d','','','','',NULL,NULL,'jinzp','','','','100','','1',''),('297e9e79511c980101511d6b23940087','友情提醒\r\n','<p>\r\n	系统上线一段时间了，感谢大家的使用和配合。\r\n</p>\r\n<p>\r\n	系统数据日益增加，为了保障系统的稳定和大家工作的便捷。\r\n</p>\r\n<p>\r\n	系统管理员友情提醒大家在使用过程中注意以下几点：\r\n</p>\r\n<p>\r\n	<span style=\"color:#e53333;\">*输入金额的时候，按照输入框格式填写数值，不要填写单位！</span> \r\n</p>\r\n<p>\r\n	<span style=\"color:#e53333;\">*输入框不要随意复制大于50字以上的内容（有特殊要求</span><span style=\"color:#e53333;\">，请联系管理员</span><span style=\"color:#e53333;\">）！</span> \r\n</p>\r\n<p>\r\n	<span style=\"color:#e53333;\">*不要随意删除数据，涉及到删除等权限的功能，确认好之后再操作，以免丢失数据！</span> \r\n</p>','1262275200000','2808662399000','0','4028808113debac70113dec10a4d0005','1447898013806','4028808113debac70113dec10a4d0005','1447897950000','402881e529a111e20129a129f4f20004','402881e53509f80b01350a5ed26b0004','class=\'qianlan_bd\'','','','',NULL,NULL,'admin','','','','','','','');

/*Table structure for table `kms_affiche_user` */

DROP TABLE IF EXISTS `kms_affiche_user`;

CREATE TABLE `kms_affiche_user` (
  `ID` varchar(32) default NULL,
  `AFFICHE_ID` varchar(32) NOT NULL,
  `USER_ID` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `kms_affiche_user` */

insert  into `kms_affiche_user`(`ID`,`AFFICHE_ID`,`USER_ID`) values ('402880923fc8972b013fc8d91cfb0016','402880923fc8972b013fc8d7a4a40014','0000'),('402880923fc8972b013fc8d9c07d0018','402880923fc8972b013fc8d9c0780017','0000'),('402880923fc8972b013fc8f2106b0020','402880923fc8972b013fc8e051a60019','0000'),('402880e440685199014068c05c740010','402880e440685199014068c05c52000f','0000'),('297e9e79511c980101511d7260730099','297e9e79511c980101511d6b23940087','0000');

/*Table structure for table `loginlog` */

DROP TABLE IF EXISTS `loginlog`;

CREATE TABLE `loginlog` (
  `ID` varchar(50) NOT NULL,
  `LOGINTIME` varchar(50) default NULL,
  `LOGOUTTIME` varchar(50) default NULL,
  `ONLINECOUNT` varchar(50) default NULL,
  `IPADDRESS` varchar(50) default NULL,
  `USER_ID` varchar(50) default NULL,
  `USER_NAME` varchar(50) default NULL,
  `STATE` varchar(10) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `loginlog` */

insert  into `loginlog`(`ID`,`LOGINTIME`,`LOGOUTTIME`,`ONLINECOUNT`,`IPADDRESS`,`USER_ID`,`USER_NAME`,`STATE`) values ('8a8189e857277c8f0157277cad400001','2016-09-14 14:57:44','2016-09-14 14:57:44','0','0:0:0:0:0:0:0:1','4028808113debac70113dec10a4d0005','管理员','1'),('402881e8572cd89601572d1f10760002','2016-09-15 17:13:13','2016-09-15 17:13:13','0','0:0:0:0:0:0:0:1','4028808113debac70113dec10a4d0005','管理员','1'),('8a818989573c202401573c2043c50001','2016-09-18 15:08:49','2016-09-18 15:16:45','476000','0:0:0:0:0:0:0:1','4028808113debac70113dec10a4d0005','管理员','0');

/*Table structure for table `system_category` */

DROP TABLE IF EXISTS `system_category`;

CREATE TABLE `system_category` (
  `INFO_ID` varchar(32) NOT NULL,
  `CATEGORY_TYPE` varchar(10) default NULL,
  `CATEGORY_TYPENAME` varchar(50) default NULL,
  `CATEGORY_NAME` varchar(50) default NULL,
  `RANK` varchar(10) default NULL,
  `PARENT_ID` varchar(32) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `system_category` */

/*Table structure for table `system_dep` */

DROP TABLE IF EXISTS `system_dep`;

CREATE TABLE `system_dep` (
  `DEP_ID` varchar(32) NOT NULL,
  `DEP_CODE` varchar(100) NOT NULL,
  `DEP_NAME` varchar(100) default NULL,
  `DEP_TYPE` varchar(10) default NULL,
  `DEP_DESC` varchar(1000) default NULL,
  `DATA_TYPE` varchar(10) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `system_dep` */

insert  into `system_dep`(`DEP_ID`,`DEP_CODE`,`DEP_NAME`,`DEP_TYPE`,`DEP_DESC`,`DATA_TYPE`) values ('402881e529a111e20129a129f4f20004','001','北京总部','','公司机构说明','1'),('402881eb4d85c42f014d85edcaf20002','002','北京分公司','','','1');

/*Table structure for table `system_dep_type` */

DROP TABLE IF EXISTS `system_dep_type`;

CREATE TABLE `system_dep_type` (
  `DEPTYPE_ID` varchar(32) NOT NULL,
  `NAME` varchar(300) default NULL,
  `CODE` varchar(50) default NULL,
  `DATA_TYPE` char(1) default NULL,
  `DISCRIPTION` varchar(3000) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `system_dep_type` */

/*Table structure for table `system_dep_user` */

DROP TABLE IF EXISTS `system_dep_user`;

CREATE TABLE `system_dep_user` (
  `DPUR_ID` varchar(32) NOT NULL,
  `USER_ID` varchar(32) NOT NULL,
  `DEP_ID` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `system_dep_user` */

insert  into `system_dep_user`(`DPUR_ID`,`USER_ID`,`DEP_ID`) values ('4028808113debac70113dec10a4d0006','4028808113debac70113dec10a4d0005','402881e529a111e20129a129f4f20004');

/*Table structure for table `system_group` */

DROP TABLE IF EXISTS `system_group`;

CREATE TABLE `system_group` (
  `GROUP_ID` varchar(50) NOT NULL,
  `GROUP_DESC` varchar(500) default NULL,
  `GROUP_STATE` varchar(10) default NULL,
  `GROUP_NAME` varchar(200) default NULL,
  `group_zuzhangid` varchar(50) default NULL,
  `group_zuzhangname` varchar(50) default NULL,
  `caopan_zhuliid` varchar(50) default NULL,
  `caopan_zhuliname` varchar(50) default NULL,
  `creat_date` varchar(50) default NULL,
  `chengyuan_ids` varchar(4000) default NULL,
  `chengyuan_names` varchar(4000) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `system_group` */

/*Table structure for table `system_group_user` */

DROP TABLE IF EXISTS `system_group_user`;

CREATE TABLE `system_group_user` (
  `INFO_ID` varchar(50) NOT NULL,
  `GROUP_ID` varchar(50) default NULL,
  `USER_ID` varchar(50) default NULL,
  `DEP_ID` varchar(50) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `system_group_user` */

/*Table structure for table `system_menu` */

DROP TABLE IF EXISTS `system_menu`;

CREATE TABLE `system_menu` (
  `MENU_ID` varchar(32) NOT NULL,
  `MENU_CODE` varchar(50) NOT NULL,
  `MENU_NAME` varchar(50) NOT NULL,
  `MENU_URL` varchar(100) default NULL,
  `MENU_DESC` varchar(1000) default NULL,
  `MENU_BY1` varchar(50) default NULL,
  `MENU_BY2` varchar(50) default NULL,
  `MENU_IMGNAME` varchar(100) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `system_menu` */

insert  into `system_menu`(`MENU_ID`,`MENU_CODE`,`MENU_NAME`,`MENU_URL`,`MENU_DESC`,`MENU_BY1`,`MENU_BY2`,`MENU_IMGNAME`) values ('8a40818713a03c3c0113a048ca300006','004003','资源配置','/back/system/menu/index.jsp','资源配置','1','','/images/secondimages/menuimages/12.gif'),('8a40818713a03c3c0113a04928da0007','004002','权限分配','/back/system/role/index.jsp','系统角色管理','0','','/images/secondimages/menuimages/index_left_2.gif'),('8a40818713a03c3c0113a03df1e20001','004','系统管理','','系统管理','','0',''),('8a40818713a03c3c0113a03ec4f10002','004001','部门用户','/back/system/dep/index.jsp','部门用户管理','1','','/images/secondimages/menuimages/43.gif'),('402880001b1f258e011b1f3f50260001','004005','登录日志','/elearning/knowledge/main.jsp?depCode=000','','0','','/images/secondimages/menuimages/index_left_2.gif'),('4028805c291bbbd501291bd9dc2d0006','004001001','部门管理','/back/system/dep/index.jsp','','0','','/images/secondimages/menuimages/index_left_2.gif'),('402880001a23712f011a237665230006','002005','信息统计','','','0','','/images/secondimages/menuimages/19.gif'),('297e8a4913a4883e0113a4fd14d50039','004004','数据字典','/back/system/codetable/index.jsp','数据字典','1','','/images/secondimages/menuimages/15.gif'),('8a40818713a03c3c0113a048ca323432','001','业务模块','','','1','0','/images/secondimages/menuimages/index_left_2.gif'),('4028805c291bbbd501291bda0b6b0007','004001002','用户管理','/back/system/user/index.jsp','','0','','/skin/skinone/images/menu/point1.gif'),('4028805c291bbbd501291bda3d490008','004002001','角色管理','','','0','','/images/secondimages/menuimages/index_left_2.gif'),('4028805c291bbbd501291bda610d0009','004002002','权限分配','/back/system/role/index.jsp','','1','','/images/secondimages/menuimages/index_left_2.gif'),('8a81859d34eae73f0134eaefa2760002','004006','我的信息','','','0','','/images/secondimages/menuimages/index_left_2.gif'),('402881ee4c5ddc73014c5e2f157a000e','004007','数据备份','','','0','','/images/secondimages/menuimages/index_left_2.gif'),('402881f74ddc0eed014ddc1e3d1c0002','004001003','群组管理','','','0','','/skin/skinone/images/menu/point1.gif'),('4028808b2d3b201e012d3b4907050034','002','数据统计','','','','0',''),('402881ea4ecea69b014eceb4597d0021','003','其他操作','','','1','0',''),('402881ea4ecea69b014ececcf26f0044','003003','公告管理','','','0','','/skin/skinone/images/menu/point1.gif'),('8a8189e8572639440157266e46fc0002','001001','业务模块','','','0','','/images/secondimages/menuimages/index_left_2.gif'),('8a818989573c202401573c2578490003','004008','系统配置','/SystemPeizhi.do?method=list','','0','','/skin/skinone/images/menu/point1.gif');

/*Table structure for table `system_peizhi` */

DROP TABLE IF EXISTS `system_peizhi`;

CREATE TABLE `system_peizhi` (
  `info_id` varchar(32) NOT NULL,
  `sys_name` varchar(50) NOT NULL,
  `sys_xulie` varchar(100) NOT NULL,
  `skin_url` varchar(500) NOT NULL,
  `sys_statu` varchar(10) NOT NULL,
  `info_statu` varchar(10) NOT NULL,
  `create_time` varchar(50) NOT NULL,
  `update_time` varchar(50) NOT NULL,
  PRIMARY KEY  (`info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `system_peizhi` */

/*Table structure for table `system_role` */

DROP TABLE IF EXISTS `system_role`;

CREATE TABLE `system_role` (
  `ROLE_ID` varchar(32) NOT NULL,
  `ROLE_NAME` varchar(50) default NULL,
  `ROLE_DESC` varchar(1000) default NULL,
  `ROLE_CREATEUSER` varchar(32) default NULL,
  `ROLE_ORDER` int(11) default NULL,
  `ROLE_BY1` varchar(50) default NULL,
  `ROLE_BY2` varchar(50) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `system_role` */

insert  into `system_role`(`ROLE_ID`,`ROLE_NAME`,`ROLE_DESC`,`ROLE_CREATEUSER`,`ROLE_ORDER`,`ROLE_BY1`,`ROLE_BY2`) values ('4028808113debac70113dec14a830007','系统超级管理员','系统管理员',NULL,1,NULL,NULL),('402881ea2a36e9ea012a3746bc7b0009','工资管理员角色','具有查询所有工资统计权限','',0,'',''),('402881ea2a36e9ea012a3746d301000a','回款管理员角色','回款管理','',0,'',''),('402881f74ddc0eed014ddc2bcc1e0004','总公司管理员','','',NULL,'',''),('402881f74ddc0eed014ddc2be8120005','分公司负责人','','',NULL,'',''),('402881f74ddc0eed014ddc2c16080006','普通员工','','',NULL,'',''),('402880e44ec79773014ec7a13e8a0002','总部工资行政助理','拥有这个角色的用户能填写全公司的考勤信息，帮助算工资','',NULL,'',''),('402880e44ec79773014ec7cce58f0009','分部工资行政助理','拥有这个角色的用户能填写自己所在分公司的考勤信息，帮助算工资','',NULL,'','');

/*Table structure for table `system_role_menu` */

DROP TABLE IF EXISTS `system_role_menu`;

CREATE TABLE `system_role_menu` (
  `MENU_ID` varchar(32) NOT NULL,
  `ROLE_ID` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `system_role_menu` */

insert  into `system_role_menu`(`MENU_ID`,`ROLE_ID`) values ('8a40818713a03c3c0113a03ec4f10002','4028808113debac70113dec14a830007'),('4028805c291bbbd501291bd9dc2d0006','4028808113debac70113dec14a830007'),('4028805c291bbbd501291bda0b6b0007','4028808113debac70113dec14a830007'),('8a40818713a03c3c0113a04928da0007','4028808113debac70113dec14a830007'),('4028805c291bbbd501291bda3d490008','4028808113debac70113dec14a830007'),('4028805c291bbbd501291bda610d0009','4028808113debac70113dec14a830007'),('8a40818713a03c3c0113a03df1e20001','4028808113debac70113dec14a830007'),('8a40818713a03c3c0113a048ca300006','4028808113debac70113dec14a830007'),('297e8a4913a4883e0113a4fd14d50039','4028808113debac70113dec14a830007'),('402880001b1f258e011b1f3f50260001','4028808113debac70113dec14a830007'),('8a40818713a03c3c0113a048ca323021','4028808113debac70113dec14a830007'),('8a40818713a03c3c0113a048ca323432','4028808113debac70113dec14a830007'),('8a81859d34eae73f0134eaefa2760002','4028808113debac70113dec14a830007'),('4028808b2d3b201e012d3b4907050021','4028808113debac70113dec14a830007'),('8a40818713a03c3c0113a03df1e20001','402881f74ddc0eed014ddc2bcc1e0004'),('402880001b1f258e011b1f3f50260001','402881f74ddc0eed014ddc2bcc1e0004'),('8a40818713a03c3c0113a03df1e20001','402881f74ddc0eed014ddc2be8120005'),('402880001b1f258e011b1f3f50260001','402881f74ddc0eed014ddc2be8120005'),('8a40818713a03c3c0113a03df1e20001','402881f74ddc0eed014ddc2c16080006'),('402880001b1f258e011b1f3f50260001','402881f74ddc0eed014ddc2c16080006'),('8a40818713a03c3c0113a03df1e20001','402880e44ec79773014ec7a13e8a0002'),('402880001b1f258e011b1f3f50260001','402880e44ec79773014ec7a13e8a0002'),('8a40818713a03c3c0113a03df1e20001','402880e44ec79773014ec7cce58f0009'),('402880001b1f258e011b1f3f50260001','402880e44ec79773014ec7cce58f0009'),('8a40818713a03c3c0113a03df1e20001','402881ea2a36e9ea012a3746bc7b0009'),('402880001b1f258e011b1f3f50260001','402881ea2a36e9ea012a3746bc7b0009'),('8a40818713a03c3c0113a03df1e20001','402881ea2a36e9ea012a3746d301000a'),('402880001b1f258e011b1f3f50260001','402881ea2a36e9ea012a3746d301000a');

/*Table structure for table `system_role_user` */

DROP TABLE IF EXISTS `system_role_user`;

CREATE TABLE `system_role_user` (
  `ROLE_ID` varchar(32) NOT NULL,
  `USER_ID` varchar(32) NOT NULL,
  `DEP_ID` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `system_role_user` */

insert  into `system_role_user`(`ROLE_ID`,`USER_ID`,`DEP_ID`) values ('4028808113debac70113dec14a830007','4028808113debac70113dec10a4d0005','402881e529a111e20129a129f4f20004');

/*Table structure for table `system_tag` */

DROP TABLE IF EXISTS `system_tag`;

CREATE TABLE `system_tag` (
  `PAGE_ID` varchar(50) NOT NULL,
  `MODL_ID` varchar(50) default NULL,
  `ORDER_NO` varchar(10) default NULL,
  `TAG_NAME` varchar(100) default NULL,
  `URL` varchar(500) default NULL,
  `SHOW_INFO` varchar(500) default NULL,
  `FLAG` varchar(50) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `system_tag` */

insert  into `system_tag`(`PAGE_ID`,`MODL_ID`,`ORDER_NO`,`TAG_NAME`,`URL`,`SHOW_INFO`,`FLAG`) values ('297e8a4913a47f7b0113a480332b0003','8a40818713a03c3c0113a048ca300006','0','模块配置','/systemMenu.do?method=list','','1'),('297e8a4913a47f7b0113a480332b0006','8a40818713a03c3c0113a048ca300006','1','标签页配置','/systemTag.do?method=Query','','1'),('4028808b2d2ae25a012d2aeb7bd40002','297e8a4913a4883e0113a4fd14d50039','0','字典信息维护','/codeTable.do?method=list&type=0','','1'),('4028808b2d2b19ee012d2b3e93300009','402880001b1f258e011b1f3f50260001','0','系统登录日志','/loginLog.do?method=list','','1'),('4028808b2d37599b012d376a2f230005','4028808b2d37599b012d3761d7840004','1','栏目基本信息','jsp/module/content/content_type_baseinfo.jsp','','1'),('4028808b2d37599b012d376a5f0d0006','4028808b2d37599b012d3761d7840004','2','子栏目排序','jsp/module/content/content_directtype_sort.jsp','','1'),('4028808b2d37599b012d376a8c760007','4028808b2d37599b012d3761d7840004','0','添加直属栏目','jsp/module/content/content_add_directtype.jsp','','1'),('4028808b2d3cb88d012d3cd80d0b0002','4028808b2d3b201e012d3b4882a30007','0','友情链接管理','/jsp/module/friendslink/list.jsp','','1'),('4028808b2d883831012d889e30910008','4028808b2d3b201e012d3b4907050008','0','服务器配置','/jsp/module/serviceconf/serviceconf_list.jsp','','1'),('402881e42a1c4072012a1caa3e300005','4028805c291bbbd501291bda3d490008','0','角色信息维护','/systemRole.do?method=root','','1'),('402881e730f7fdb60130f80230380006','402881e730f7fdb60130f801f5c00005','0','积分策略','http://www.baidu.com','','1'),('402881ea2a36e9ea012a374d8154000b','4028805c291bbbd501291bda610d0009','0','对用户授权','/back/system/role/roletouser.jsp ','','1'),('402881ea2a36e9ea012a374dac9a000c','4028805c291bbbd501291bda610d0009','1','对菜单授权','/back/system/role/roletomenu.jsp','','1'),('8a81859d34eae73f0134eaf069cd0003','8a81859d34eae73f0134eaefa2760002','0','基本信息修改','/systemUser.do?method=edit','','1'),('402880e44046c6e1014046e6b3b10005','402880e44046c6e1014046e5caab0004','0','意见建议','jsp/module/yijianinfo/yijianinfo_ajax_list.jsp','','1'),('402881f74ddc0eed014ddc1fb65e0003','402881f74ddc0eed014ddc1e3d1c0002','0','群组管理','/systemGroup.do?method=list','','1'),('402881f34e95c721014e95c86fc30002','402881ee4c5ddc73014c5e2f157a000e','0','数据备份','/jsp/dbcontrol/db_bak.jsp','','1'),('402881e45075134201507517d40f0006','402881ea4ecea69b014ececcf26f0044','0','通知通告','/jsp/module/affiche/info_issuance_main.jsp','','1'),('8a8189e8572639440157266e6fc00003','8a8189e8572639440157266e46fc0002','0','业务模块','','','1');

/*Table structure for table `system_user_ext` */

DROP TABLE IF EXISTS `system_user_ext`;

CREATE TABLE `system_user_ext` (
  `ID` varchar(50) NOT NULL,
  `USER_ID` varchar(50) default NULL,
  `EXPERT_LEVEL` varchar(50) default NULL,
  `WORK_KIND` varchar(50) default NULL,
  `USERQQ` varchar(50) default NULL,
  `USERMSN` varchar(50) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `system_user_ext` */

/*Table structure for table `system_user_scheme` */

DROP TABLE IF EXISTS `system_user_scheme`;

CREATE TABLE `system_user_scheme` (
  `USER_ID` varchar(32) NOT NULL,
  `SCHEME_ID` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `system_user_scheme` */

/*Table structure for table `system_user_sql` */

DROP TABLE IF EXISTS `system_user_sql`;

CREATE TABLE `system_user_sql` (
  `USER_ID` varchar(32) NOT NULL,
  `USER_TYPE` varchar(50) default NULL,
  `USER_NAME` varchar(20) default NULL,
  `LOGIN_NAME` varchar(20) default NULL,
  `LOGIN_PASSWORD` varchar(20) default NULL,
  `USER_GENDER` varchar(20) default NULL,
  `USER_BIRTHDAY` varchar(20) default NULL,
  `USER_CARD_TYPE` varchar(50) default NULL,
  `USER_CARD_ID` varchar(20) default NULL,
  `USER_COUNTRY` varchar(20) default NULL,
  `USER_FOLK` varchar(20) default NULL,
  `USER_PROVINCE` varchar(20) default NULL,
  `USER_CITY` varchar(20) default NULL,
  `USER_ADDRESS` varchar(100) default NULL,
  `USER_ZIPCODE` varchar(10) default NULL,
  `USER_PHONE` varchar(20) default NULL,
  `USER_FAX` varchar(20) default NULL,
  `USER_EMAIL` varchar(50) default NULL,
  `IS_LOGIN` varchar(10) default NULL,
  `USER_JOB` varchar(50) default NULL,
  `USER_DEGREE` varchar(50) default NULL,
  `USER_MEMO` varchar(1000) default NULL,
  `USER_ISVALID` char(1) default NULL,
  `MOBILE_PHONE` varchar(50) default NULL,
  `CREATE_DATE` varchar(50) default NULL,
  `MASTERCOUNT` int(11) default NULL,
  `USER_JINBI` varchar(30) default NULL,
  `USER_NICHENG` varchar(20) default NULL,
  `LIZHI_TIME` varchar(50) default NULL,
  `RUZHI_TIME` varchar(50) default NULL,
  `YUEDING_DIXIN` varchar(50) default NULL,
  `yewu_rolr_name` varchar(100) default NULL,
  `yewu_role_id` varchar(50) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `system_user_sql` */

insert  into `system_user_sql`(`USER_ID`,`USER_TYPE`,`USER_NAME`,`LOGIN_NAME`,`LOGIN_PASSWORD`,`USER_GENDER`,`USER_BIRTHDAY`,`USER_CARD_TYPE`,`USER_CARD_ID`,`USER_COUNTRY`,`USER_FOLK`,`USER_PROVINCE`,`USER_CITY`,`USER_ADDRESS`,`USER_ZIPCODE`,`USER_PHONE`,`USER_FAX`,`USER_EMAIL`,`IS_LOGIN`,`USER_JOB`,`USER_DEGREE`,`USER_MEMO`,`USER_ISVALID`,`MOBILE_PHONE`,`CREATE_DATE`,`MASTERCOUNT`,`USER_JINBI`,`USER_NICHENG`,`LIZHI_TIME`,`RUZHI_TIME`,`YUEDING_DIXIN`,`yewu_rolr_name`,`yewu_role_id`) values ('4028808113debac70113dec10a4d0005','','管理员','admin','123','2','1980-10-04','','','','','','','','100029','','','','','','8a8185a538dabcec0138dae673860004','','1','','2015-03-28',0,'10000','超级管理员','','','','','');

/*Table structure for table `t_user` */

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `INFO_ID` varchar(32) NOT NULL,
  `C_CORP` varchar(150) NOT NULL,
  `C_PASS` varchar(150) NOT NULL,
  PRIMARY KEY  (`INFO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `t_user` */

insert  into `t_user`(`INFO_ID`,`C_CORP`,`C_PASS`) values ('8a8185a538dabcec0138dacab1260003','a','aa');

/*Table structure for table `xiaoxi_info` */

DROP TABLE IF EXISTS `xiaoxi_info`;

CREATE TABLE `xiaoxi_info` (
  `info_id` varchar(32) NOT NULL,
  `xx_neirong` varchar(500) NOT NULL,
  `xx_type` varchar(50) NOT NULL,
  `xx_url` varchar(500) NOT NULL,
  `xx_create` varchar(50) NOT NULL,
  `xx_statu` varchar(10) NOT NULL,
  `xx_senduserid` varchar(50) default '',
  `xx_sendusername` varchar(50) default '',
  `xx_recuserid` varchar(50) default '',
  `xx_recusername` varchar(50) default '',
  PRIMARY KEY  (`info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `xiaoxi_info` */

insert  into `xiaoxi_info`(`info_id`,`xx_neirong`,`xx_type`,`xx_url`,`xx_create`,`xx_statu`,`xx_senduserid`,`xx_sendusername`,`xx_recuserid`,`xx_recusername`) values ('297e9e79516528cf0151652f8f120003','新的建仓信息','jc-caopan-jc','','2015-12-03 08:13:14','1','4028808113debac70113dec10a4d0005','管理员','402881f74edcdaa6014edd5143d10024','戴会'),('297e9e79516528cf0151666709d50127','新的建仓信息','jc-caopan-jc','','2015-12-03 13:53:27','1','297e9e79510ed3bc01511423c9800451','姜华','402881f74de08ff6014de0a27a8c0007','严海涛'),('297e9e7951762d8101517a690e80011c','新的建仓信息','jc-caopan-jc','','2015-12-07 11:08:04','1','402881f14e7febf3014e7ffaa4300017','陈海燕','402881f74de08ff6014de0a27a8c0007','严海涛'),('297e9e7951762d8101517af5ad8e0198','新的建仓信息','jc-caopan-jc','','2015-12-07 13:41:39','1','402881f14e7febf3014e7ffc262c001b','郭亚楠','402881f74de5b255014de5d2d320000c','杨新'),('297e9e795181e380015185c67d8c01c2','新的建仓信息','jc-caopan-jc','','2015-12-09 16:05:56','1','4028808113debac70113dec10a4d0005','管理员','402881f74edcdaa6014edd5143d10024','戴会'),('297e9e795181e380015185c84bc501c6','新的建仓信息','jc-caopan-jc','','2015-12-09 16:07:55','1','4028808113debac70113dec10a4d0005','管理员','402881f74edcdaa6014edd5143d10024','戴会'),('297e9e795181e380015185ced33e01d4','新的建仓信息','jc-caopan-jc','','2015-12-09 16:15:02','1','4028808113debac70113dec10a4d0005','管理员','402881f74edcdaa6014edd5143d10024','戴会'),('297e9e795181e38001518fef017c06c2','新的建仓信息','jc-caopan-jc','','2015-12-11 15:26:24','1','297e9e795181e380015184ab09fe008b','刘少东','297e9e795137463b015138037d500160','贾凤玲'),('297e9e795181e38001519e29b07907b9','新的建仓信息','jc-caopan-jc','','2015-12-14 09:45:11','1','297e9e79510ed3bc01510ee02c2b003a','莫宏昌','297e9e795137463b015138037d500160','贾凤玲'),('297e9e795181e38001519e3a065f07eb','新的建仓信息','jc-caopan-jc','','2015-12-14 10:03:01','1','297e9e79510ed3bc01510ee02c2b003a','莫宏昌','297e9e795137463b015138037d500160','贾凤玲'),('297e9e795181e38001519e65a37d0827','新的建仓信息','jc-caopan-jc','','2015-12-14 10:50:39','1','297e9e79511c980101511f1f967f0324','陈小武','297e9e795137463b015138037d500160','贾凤玲'),('297e9e795181e38001519e8797920851','新的建仓信息','jc-caopan-jc','','2015-12-14 11:27:45','1','297e9e7951762d8101517a7579ac0125','陈丽丽','297e9e795137463b015138037d500160','贾凤玲'),('297e9e795181e38001519eda6ee7086a','新的建仓信息','jc-caopan-jc','','2015-12-14 12:58:14','1','297e9e79511c980101512389539a051f','赵天官','297e9e795137463b015138037d500160','贾凤玲'),('297e9e795181e38001519ef27ca60890','新的建仓信息','jc-caopan-jc','','2015-12-14 13:24:30','1','297e9e79511c980101512389539a051f','赵天官','297e9e795137463b015138037d500160','贾凤玲'),('297e9e795181e38001519ef2b0640892','新的建仓信息','jc-caopan-jc','','2015-12-14 13:24:43','1','297e9e79511c980101512389539a051f','赵天官','297e9e795137463b015138037d500160','贾凤玲'),('297e9e795181e38001519f01e75d08a5','新的建仓信息','jc-caopan-jc','','2015-12-14 13:41:20','1','297e9e79511c980101512389539a051f','赵天官','297e9e795137463b015138037d500160','贾凤玲'),('297e9e795181e38001519f1aaea308d5','新的建仓信息','jc-caopan-jc','','2015-12-14 14:08:24','1','297e9e795157db7f01515c561ef5024c','李妮','297e9e795137463b015138037d500160','贾凤玲'),('297e9e795181e38001519fbeece00947','新的建仓信息','jc-caopan-jc','','2015-12-14 17:07:48','1','297e9e7951762d8101517a7579ac0125','陈丽丽','297e9e795137463b015138037d500160','贾凤玲');

/*Table structure for table `yijian_info` */

DROP TABLE IF EXISTS `yijian_info`;

CREATE TABLE `yijian_info` (
  `INFO_ID` varchar(32) NOT NULL,
  `YIJIAN_CONTENT` varchar(1000) NOT NULL,
  `YIJIAN_CREATOR_ID` varchar(32) NOT NULL default '',
  `YIJIAN_CREATOR_NAME` varchar(10) NOT NULL default '',
  `YIJIAN_CREATE_DATE` varchar(50) NOT NULL,
  PRIMARY KEY  (`INFO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `yijian_info` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
